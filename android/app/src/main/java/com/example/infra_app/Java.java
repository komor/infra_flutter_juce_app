package com.rmsl.juce;

import android.content.Context;

public class Java {

    static {
        System.loadLibrary ("infra_lib");
    }

    public native static void initialiseJUCE (Context appContext);
    public native static void start_infra();

}
