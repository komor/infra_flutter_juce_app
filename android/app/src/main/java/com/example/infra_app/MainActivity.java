package com.example.infra_app;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

import com.rmsl.juce.Java;

public class MainActivity extends FlutterActivity {

    private static final String CHANNEL = "infra_channel";

    @Override
    public void configureFlutterEngine(FlutterEngine flutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);

        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
                .setMethodCallHandler((call, result) -> {

                    if (call.method.equals("init_infra")) {
                        // juce init threads
                        Java.initialiseJUCE(this.getContext());
                        Java.start_infra();
                    }
                });
    }
}
