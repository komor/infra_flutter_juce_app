/*
  ==============================================================================

    Core.cpp
    Created: 28 Jul 2020 12:07:21pm
    Author:  luke

  ==============================================================================
*/

#include "Core.h"

Core::Core() : deviceManager(defaultDeviceManager)
{

  // Some platforms require permissions to open input channels so request that here
  if (juce::RuntimePermissions::isRequired(juce::RuntimePermissions::recordAudio) && !juce::RuntimePermissions::isGranted(juce::RuntimePermissions::recordAudio))
  {
    juce::RuntimePermissions::request(juce::RuntimePermissions::recordAudio,
                                      [&](bool granted) { setAudioChannels(granted ? 2 : 0, 2); });
  }
  else
  {
    // Specify the number of input and output channels that we want to open
    setAudioChannels(2, 2);
  }

  //setBufferSize(4096);
  //getAvaibleBufferSizes();
  //Logger::outputDebugString(String(getBufferSize()));

}

Core::~Core()
{

  shutdownAudio();
}

Array<int> Core::getAvaibleBufferSizes()
{

  auto dev = deviceManager.getCurrentAudioDevice();
  //Logger::outputDebugString(String(dev->getName()));
  return dev->getAvailableBufferSizes();
}

void Core::setBufferSize(int size)
{

  auto devSetup = deviceManager.getAudioDeviceSetup();
  devSetup.bufferSize = size;
  deviceManager.setAudioDeviceSetup(devSetup, true);
}

int Core::getBufferSize()
{

  return deviceManager.getAudioDeviceSetup().bufferSize;
}

void Core::setAudioChannels(int numInputs, int numOutputs)
{

  String audioError;

  audioError = deviceManager.initialise(numInputs, numOutputs, nullptr, true);

  jassert(audioError.isEmpty());

  deviceManager.addAudioCallback(&audioSourcePlayer);
  audioSourcePlayer.setSource(this);
}

void Core::shutdownAudio()
{
  audioSourcePlayer.setSource(nullptr);
  deviceManager.removeAudioCallback(&audioSourcePlayer);

  // other audio callbacks may still be using the device

  deviceManager.closeAudioDevice();
}

void Core::releaseResources()
{
}

void Core::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
  currentSampleRate = sampleRate;
  updateF2();
  updateAngleDelta1();
  updateAngleDelta2();
}

// sound functions
void Core::getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill)
{
  auto *leftBuffer = bufferToFill.buffer->getWritePointer(0, bufferToFill.startSample);
  auto *rightBuffer = bufferToFill.buffer->getWritePointer(1, bufferToFill.startSample);

  // left
  auto localTargetFrequency = targetFrequency1;
  if (targetFrequency1 != currentFrequency1)
  {
    auto frequencyIncrement = (targetFrequency1 - currentFrequency1) / bufferToFill.numSamples;

    for (auto sample = 0; sample < bufferToFill.numSamples; ++sample)
    {
      auto currentSample = (float)std::sin(currentAngle1);
      currentFrequency1 += frequencyIncrement;
      updateAngleDelta1();
      currentAngle1 += angleDelta1;
      leftBuffer[sample] = currentSample;

      if (!binaural)
        rightBuffer[sample] = currentSample;
    }

    currentFrequency1 = localTargetFrequency;
  }
  else
  {
    for (auto sample = 0; sample < bufferToFill.numSamples; ++sample)
    {
      auto currentSample = (float)std::sin(currentAngle1);
      currentAngle1 += angleDelta1;
      leftBuffer[sample] = currentSample;

      if (!binaural)
        rightBuffer[sample] = currentSample;
    }
  }

  // right
  localTargetFrequency = targetFrequency2;
  if (targetFrequency2 != currentFrequency2)
  {
    auto frequencyIncrement = (targetFrequency2 - currentFrequency2) / bufferToFill.numSamples;

    for (auto sample = 0; sample < bufferToFill.numSamples; ++sample)
    {
      auto currentSample = (float)std::sin(currentAngle2);
      currentFrequency2 += frequencyIncrement;
      updateAngleDelta2();
      currentAngle2 += angleDelta2;

      if (binaural)
      {
        //   leftBuffer[sample] += currentSample;
        rightBuffer[sample] = currentSample;
      }
      else
      {
        leftBuffer[sample] += currentSample;
        rightBuffer[sample] += currentSample;
      }
    }

    currentFrequency2 = localTargetFrequency;
  }
  else
  {
    for (auto sample = 0; sample < bufferToFill.numSamples; ++sample)
    {
      auto currentSample = (float)std::sin(currentAngle2);
      currentAngle2 += angleDelta2;

      if (binaural)
      {
        //     leftBuffer[sample] += currentSample;
        rightBuffer[sample] = currentSample;
      }
      else
      {
        leftBuffer[sample] += currentSample;
        rightBuffer[sample] += currentSample;
      }
    }
  }

  auto localTargetLevel = targetLevel;
  bufferToFill.buffer->applyGainRamp(bufferToFill.startSample, bufferToFill.numSamples, currentLevel, localTargetLevel);
  currentLevel = localTargetLevel;
}

inline void Core::updateAngleDelta1()
{
  auto cyclesPerSample = currentFrequency1 / currentSampleRate;
  angleDelta1 = cyclesPerSample * 2.0 * juce::MathConstants<double>::pi;
}

inline void Core::updateAngleDelta2()
{
  auto cyclesPerSample = currentFrequency2 / currentSampleRate;
  angleDelta2 = cyclesPerSample * 2.0 * juce::MathConstants<double>::pi;
}

void Core::updateF2()
{
  if (stepByHz)
  {

    targetFrequency2 = diff_Hz + targetFrequency1;
  }
  else
  {

    targetFrequency2 = semi_map[diff_Hz] * targetFrequency1;
  }
}