
/*
  ==============================================================================

    Core.h
    Created: 28 Jul 2020 12:07:29pm
    Author:  luke

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>
#include <jni.h>

using namespace juce;

const double semi_map[] = {
    1.0,
    1.05946309436,
    1.12246204831,
    1.189207115,
    1.25992104989,
    1.33483985417,
    1.41421356237,
    1.49830707688,
    1.58740105197,
    1.68179283051,
    1.78179743628,
    1.88774862536,
    2.0};

class Core : public AudioSource
{
public:
  Core();
  ~Core();

  void setAudioChannels(int numInputs, int numOutputs);
  void prepareToPlay(int samplesPerBlockExpected, double sampleRate);
  void releaseResources();
  void getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill);

  void shutdownAudio();

  Array<int> getAvaibleBufferSizes();
  void setBufferSize(int size);
  int getBufferSize();

  void setFreq1(double freq1) {
    targetFrequency1 = freq1;
    updateF2();
  }

  double getFreq1() { return targetFrequency1; }
  double getFreq2() { return targetFrequency2; }

  void setDiff(int diff) {
    diff_Hz = diff;
    updateF2();
  }
  int getDiff() { return diff_Hz; }

  void setBinaural(bool binaural_mode) { binaural = binaural_mode; }
  bool getBinaural() { return binaural; }

  void setByHz(bool byHz) { stepByHz = byHz; }
  bool getByHz() { return stepByHz; }

  AudioDeviceManager &deviceManager;

private:

  AudioDeviceManager defaultDeviceManager;
  AudioSourcePlayer audioSourcePlayer;

  // audio generation

  inline void updateAngleDelta1();
  inline void updateAngleDelta2();

  void updateF2();

  int diff_Hz = 0;
  bool stepByHz = false;
  bool binaural = false;

  double currentSampleRate = 0.0;
  double currentAngle1 = 0.0;
  double angleDelta1 = 0.0;

  double currentAngle2 = 0.0;
  double angleDelta2 = 0.0;

  double currentFrequency1 = 311.0;
  double targetFrequency1 = 311.0;
  double currentFrequency2 = 200.0;
  double targetFrequency2 = 200.0;

  float currentLevel = 0.1f;
  float targetLevel = 0.1f;
};

static Core *core;

extern "C"
{
  void JNICALL Java_com_rmsl_juce_Java_start_1infra(JNIEnv *, jobject)
  {
    core = new Core();
  }

  void stop_infra()
  {
    delete core;
  }

  void set_frequency(double freq)
  {
    core->setFreq1(freq);
  }

  double get_frequency()
  {
    return core->getFreq1();
  }

  double get_diff_frequency() {
    return core->getFreq2();
  }

  void set_diff(int diff)
  {
    core->setDiff(diff);
  }

  int get_diff()
  {
    return core->getDiff();
  }

  void set_binaural_mode(bool mode)
  {
    core->setBinaural(mode);
  }

  bool get_binaural_mode()
  {
    return core->getBinaural();
  }

  void set_by_Hz(bool mode)
  {
    core->setByHz(mode);
  }

  bool get_by_Hz()
  {
    return core->getByHz();
  }
  
}
