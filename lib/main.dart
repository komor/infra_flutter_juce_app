import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:infra_app/widgets/defines.dart';
import 'dart:ffi'; // For FFI
import 'dart:io'; // For Platform.isX

import 'package:infra_app/widgets/horizontal_slider.dart';
import 'package:infra_app/widgets/rotary_slider.dart';

final DynamicLibrary infraChannelF = Platform.isAndroid
    ? DynamicLibrary.open("libinfra_lib.so")
    : DynamicLibrary.process();

void main() {
  runApp(InfraApp());
}

class InfraApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  static const infraChannel = const MethodChannel('infra_channel');
  double _frequency;
  double _diff_frequency;
  int _diff_pos;

  Future<String> initInfra() async {
    return await infraChannel.invokeMethod('init_infra');
  }

  void Function(double freq) setFrequency = infraChannelF
      .lookup<NativeFunction<Void Function(Double)>>("set_frequency")
      .asFunction();

  void Function(int diff) setDiff = infraChannelF
      .lookup<NativeFunction<Void Function(Int32)>>("set_diff")
      .asFunction();

  double Function() getDiffFreq = infraChannelF
      .lookup<NativeFunction<Double Function()>>("get_diff_frequency")
      .asFunction();

  void frequencyChanged(int dir) {
    setState(() {
      if (dir > 0) {
        if (_frequency < 1000) _frequency++;
      } else {
        if (_frequency > 50) _frequency--;
      }
    });
    setFrequency(_frequency);
    _diff_frequency = getDiffFreq();
  }

  void diffChanged(double diff) {
    setDiff(diff.toInt());
    setState(() {
      _diff_frequency = getDiffFreq();
      _diff_pos = diff.toInt();
    });
  }

  @override
  void initState() {
    initInfra();
    _frequency = 311.0;
    _diff_frequency = 311;
    _diff_pos = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      //fit: StackFit.expand,
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(color: darkGrey.color),
        ),
        Column(
          //verticalDirection: VerticalDirection.down,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //Container(height: MediaQuery.of(context).size.height * 0.8),

            Container(
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        //padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Text("Interval",
                            style: GoogleFonts.doHyeon(
                                textStyle: TextStyle(
                                    color: Colors.white, fontSize: 18))),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text("${str_map[_diff_pos]}",
                            style: GoogleFonts.doHyeon(
                                textStyle: TextStyle(
                                    color: Colors.white, fontSize: 25))),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              //height: 120,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: HorizontalSlider(
                  changedCallBack: diffChanged,
                ),
              ),
            ),
            //  Container(height: 20),

            Container(
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        //padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Text("Frequency",
                            style: GoogleFonts.doHyeon(
                                textStyle: TextStyle(
                                    color: Colors.white, fontSize: 18))),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
//                        child: TextField(
//                        decoration: InputDecoration(
//                            labelText: "${_frequency.round().toString()} Hz",
//                            border: InputBorder.none,
//
//                            suffixText: ' Hz',
//                        ),
//                        keyboardType: TextInputType.number,
//                        inputFormatters: <TextInputFormatter>[
//                       // FilteringTextInputFormatter.digitsOnly
//                        ], // Only numbers can be entered

                        child: Text("${_frequency.round().toString()} Hz",
                            style: GoogleFonts.doHyeon(
                                textStyle: TextStyle(
                                    color: Colors.white, fontSize: 25))),
//                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.width * 0.8,
              width: MediaQuery.of(context).size.width * 0.8,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: RotarySlider(
                  changedCallBack: frequencyChanged,
                  radius: (MediaQuery.of(context).size.width * 0.25).toInt(),
                ),
              ),
            ),
          ],
        ),
      ],
    ));
  }
}
