import 'dart:ui';
import 'package:tinycolor/tinycolor.dart';

TinyColor lightGrey = TinyColor(Color(0xFF001132));
TinyColor darkGrey = TinyColor(Color(0xFF000D27));

List<String> str_map =
[
"P1",
"m2",
"M2",
"m3",
"M3",
"P4",

"d5",
"P5",
"m6",
"M6",
"m7",
"M7",
"P8"
];