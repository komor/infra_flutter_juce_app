import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'defines.dart';

class CustomSliderThumbRect extends SliderComponentShape {
  final double thumbRadius;
  final thumbHeight;
  final int min;
  final int max;

  const CustomSliderThumbRect({
    this.thumbRadius = 0,
    this.thumbHeight = 80,
    this.min,
    this.max,
  });

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size.fromRadius(thumbRadius);
  }

  @override
  void paint(
    PaintingContext context,
    Offset center, {
    Animation<double> activationAnimation,
    Animation<double> enableAnimation,
    bool isDiscrete,
    TextPainter labelPainter,
    RenderBox parentBox,
    SliderThemeData sliderTheme,
    TextDirection textDirection,
    double value,
  }) {
    final Canvas canvas = context.canvas;

    final rRect = RRect.fromRectAndRadius(
      Rect.fromCenter(
          center: center, width: thumbHeight * .7, height: thumbHeight * .7),
      Radius.circular(thumbRadius * 6),
    );

    Rect rect = new Rect.fromCenter(
      center: center, width: thumbHeight * 1.2, height: thumbHeight * .6);

    final Gradient gradient = new LinearGradient(
      colors: <Color>[
        darkGrey.brighten(14).color,
   //     darkGrey.brighten(20).color,
    //    darkGrey.brighten(18).color,
        darkGrey.brighten(15).color,
      ],
      stops: [
        0.0,
      //  0.5,
     //   0.6,
        1.0,
      ],
      begin: const FractionalOffset(0.0, 0.0),
      end: const FractionalOffset(1.0, 1.00),
    );

    final paint = Paint()
      ..shader = gradient.createShader(rect)
     // ..maskFilter = MaskFilter.blur(BlurStyle.normal, convertRadiusToSigma(4))
      ..color = darkGrey.brighten(18).color
      ..style = PaintingStyle.fill;

//    TextSpan span = new TextSpan(
//        style: GoogleFonts.ubuntu(textStyle: TextStyle(color: Colors.white, fontSize: 20)),
//        text: str_map[getValue(value)]);
//    TextPainter tp = new TextPainter(
//        text: span,
//        textAlign: TextAlign.left,
//        textDirection: TextDirection.ltr);
//    tp.layout();
//    Offset textCenter =
//        Offset(center.dx - (tp.width / 2), center.dy - (tp.height / 2));

    Size size = Size(thumbHeight * 1.2, thumbHeight * .6);

//    canvas.drawPath(Path()
//      ..addRect(
//          Rect.fromCenter(
//              center: Offset(center.dx + 3 , center.dy + 2
//              ), width: thumbHeight * 1.0, height: thumbHeight * 0.5))
//     // ..addOval(
//     //     Rect.fromPoints(Offset(0, 0), Offset(size.width, size.height)))
//      ..fillType = PathFillType.evenOdd,
//        Paint()
//          ..color= Colors.black.withAlpha(255));
//        //  ..maskFilter = MaskFilter.blur(BlurStyle.normal, convertRadiusToSigma(12)));

//    canvas.drawPath(Path()
//      ..addRect(
//          Rect.fromCenter(
//              center: Offset(center.dx - 3 , center.dy - 2
//              ), width: thumbHeight * 1.0, height: thumbHeight * 0.5))
//    // ..addOval(
//    //     Rect.fromPoints(Offset(0, 0), Offset(size.width, size.height)))
//      ..fillType = PathFillType.evenOdd,
//        Paint()
//          ..color= Colors.white.withAlpha(180));
//         // ..maskFilter = MaskFilter.blur(BlurStyle.normal, convertRadiusToSigma(12)));

    canvas.drawRRect(rRect, paint);
    //tp.paint(canvas, textCenter);
  }

  static double convertRadiusToSigma(double radius) {
    return radius * 0.57735 + 0.5;
  }

  int getValue(double value) {
    return ((max) * (value)).round();
  }
}

class HorizontalSlider extends StatefulWidget {
  final double sliderHeight;
  final int min;
  final int max;
  final fullWidth;
  final ValueChanged<double> changedCallBack;

  HorizontalSlider(
      {
      this.sliderHeight = 70,
      this.max = 12,
      this.min = 0,
      this.fullWidth = true,
      this.changedCallBack
      });



  @override
  _HorizontalSliderState createState() => _HorizontalSliderState();
}

class _HorizontalSliderState extends State<HorizontalSlider> {
  double _value = 0;

  @override
  Widget build(BuildContext context) {
    double paddingFactor = .2;

    if (this.widget.fullWidth) paddingFactor = .3;

    return Container(
      width: this.widget.fullWidth
          ? double.infinity
          : (this.widget.sliderHeight) * 5.5,
      height: (this.widget.sliderHeight),
      decoration: new BoxDecoration(

          borderRadius: new BorderRadius.all(
            Radius.circular((this.widget.sliderHeight * .3)),
          ),
            color : darkGrey.color,
//          gradient: new LinearGradient(
//              colors: [
//                darkGrey.brighten(26).color.withOpacity(0.4),
//                //darkGrey.brighten(18).color,
//                //darkGrey.brighten(18).color,
//                darkGrey.brighten(20).color.withOpacity(0.2),
//              ],
//              begin: const FractionalOffset(0.0, 0.0),
//              end: const FractionalOffset(1.0, 1.00),
//              stops: [0.0,
//                //0.3,
//                //0.6,
//                1.0],
//              tileMode: TileMode.mirror),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.4),
           //   spreadRadius: 4,
              blurRadius: 10,
              offset: Offset(3, 1),
            ),
//            BoxShadow(
//              color: Colors.white.withOpacity(0.2),
//            //  spreadRadius: 5,
//              blurRadius: 12,
//              offset: Offset(-3, -3),
//            ),

          ]),
      child: Padding(
        padding: EdgeInsets.fromLTRB(this.widget.sliderHeight * paddingFactor,
            2, this.widget.sliderHeight * paddingFactor, 2),
        child: Row(
          children: <Widget>[
//            Text(
//              '${this.widget.min}',
//              textAlign: TextAlign.center,
//              style: TextStyle(
//                fontSize: this.widget.sliderHeight * .3,
//                fontWeight: FontWeight.w700,
//                color: Colors.white,
//
//              ),
//            ),
            SizedBox(
              width: this.widget.sliderHeight * .1,
            ),
            Expanded(
              child: Center(
                child: SliderTheme(
                  data: SliderTheme.of(context).copyWith(
                    activeTrackColor: Colors.white.withOpacity(0),
                    inactiveTrackColor: Colors.white.withOpacity(0),
                    //overlayShape: ,
                    trackHeight: 5.0,
                    thumbColor: Colors.white,
                    //rangeTrackShape: ,
                    thumbShape: CustomSliderThumbRect(
                      thumbRadius: this.widget.sliderHeight * .4,
                      min: this.widget.min,
                      max: this.widget.max,
                    ),
                    overlayColor: Colors.white.withOpacity(0),
                    //valueIndicatorColor: Colors.white,
                    activeTickMarkColor: Colors.white,
                    inactiveTickMarkColor: Colors.white.withOpacity(0.5),
                  ),
                  child: Slider(
                      divisions: 12,
                      min: 0,
                      max: 12,
                      value: _value,
                      onChanged: (value) {
                        setState(() {
                          widget.changedCallBack(value);
                          HapticFeedback.vibrate();
                          _value = value;
                         // _onChanged;
                        });
                      }),
                ),
              ),
            ),
            SizedBox(
              width: this.widget.sliderHeight * .1,
            ),
//            Text(
//              '${this.widget.max}',
//              textAlign: TextAlign.center,
//              style: TextStyle(
//                fontSize: this.widget.sliderHeight * .3,
//                fontWeight: FontWeight.w700,
//                color: Colors.white,
//              ),
//            ),
          ],
        ),
      ),
    );
  }
}

//class HorizontalSlider extends StatefulWidget {
//  HorizontalSlider({Key key}) : super(key: key);
//
//  @override
//  _HorizontalSliderState createState() => _HorizontalSliderState();
//}
//
//class _HorizontalSliderState extends State<HorizontalSlider> {
//
//  double diffHz;
//
//  @override
//  void initState() {
//    diffHz = 0;
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Slider(
//      min: 0,
//      divisions: 12,
//      max: 12,
//      value: diffHz,
//      onChanged: (val) {
//        setState(() {
//          HapticFeedback.vibrate();
//          diffHz = val;
//        });
//      },
//    );
//  }
