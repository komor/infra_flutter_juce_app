import 'dart:math';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'defines.dart';

double convertRadiusToSigma(double radius) {
  return radius * 0.57735 + 0.5;
}

class BasePainter extends CustomPainter {
  Color baseColor;

  Offset center;
  double radius;

  BasePainter({this.baseColor});

  final Gradient gradient = new LinearGradient(
    colors: <Color>[
      darkGrey.brighten(13).color,
      //darkGrey.brighten(15).color,
      //darkGrey.brighten(15).color,
      darkGrey.brighten(14).color,
    ],
    stops: [
      0.0,
      // 0.5,
      // 0.6,
      1.0,
    ],
    begin: const FractionalOffset(0.0, 0.0),
    end: const FractionalOffset(1.0, 1.00),
  );

  @override
  void paint(Canvas canvas, Size size) {
    center = Offset(size.width / 2, size.height / 2);
    radius = min(size.width / 2, size.height / 2);

    Rect rect = new Rect.fromCenter(
        center: center, width: size.width, height: size.height);

    Paint paint = Paint()
      ..color = darkGrey.color
    //  ..shader = gradient.createShader(rect)
     // ..maskFilter = MaskFilter.blur(BlurStyle.normal, convertRadiusToSigma(4))
      ..style = PaintingStyle.fill;

    canvas.drawPath(
        Path()
          ..addOval(Rect.fromCenter(
              center: Offset(center.dx , center.dy ),
              width: size.width,
              height: size.height))
          // ..addOval(
          //     Rect.fromPoints(Offset(0, 0), Offset(size.width, size.height)))
          ..fillType = PathFillType.evenOdd,
        Paint()
          ..color = Colors.black.withAlpha(100)
          ..maskFilter =
              MaskFilter.blur(BlurStyle.normal, convertRadiusToSigma(15)));

//    canvas.drawPath(
//        Path()
//          ..addOval(Rect.fromCenter(
//              center: Offset(center.dx - 3, center.dy - 2),
//              width: size.width,
//              height: size.height))
//          // ..addOval(
//          //     Rect.fromPoints(Offset(0, 0), Offset(size.width, size.height)))
//          ..fillType = PathFillType.evenOdd,
//        Paint()
//          ..color = Colors.white.withAlpha(70)
//          ..maskFilter =
//              MaskFilter.blur(BlurStyle.normal, convertRadiusToSigma(12)));

    canvas.drawCircle(center, radius, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class SliderPainter extends CustomPainter {
  Color baseColor;

  double width;
  double height;

  Offset offsetPos;
  double currentAngle;

  SliderPainter(
      {@required this.offsetPos, @required this.height, @required this.width});

  @override
  void paint(Canvas canvas, Size size) {
    Path path = Path();
    Offset center = Offset(size.width / 2, size.height / 2);

    path.addRRect(
      RRect.fromRectAndCorners(
        Rect.fromCenter(
            center: Offset(center.dx + offsetPos.dx, center.dy + offsetPos.dy),
            height: height,
            width: width),
        topLeft: Radius.circular(40),
        topRight: Radius.circular(40),
        bottomLeft: Radius.circular(40),
        bottomRight: Radius.circular(40),
      ),
    );

    Paint paint = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill;

//    canvas.drawPath(
//        Path()
//          ..addRRect(RRect.fromRectAndCorners(
//              Rect.fromCenter(
//                  center: Offset(center.dx + offsetPos.dx + 3,
//                      center.dy + offsetPos.dy + 3),
//                  height: height,
//                  width: width),
//              topLeft: Radius.circular(20),
//              topRight: Radius.circular(20),
//              bottomLeft: Radius.circular(20),
//              bottomRight: Radius.circular(20)))
//          ..fillType = PathFillType.evenOdd,
//        Paint()
//          ..color = Colors.white.withAlpha(50)
//          ..maskFilter =
//              MaskFilter.blur(BlurStyle.normal, convertRadiusToSigma(10)));

    canvas.drawPath(
        Path()
          ..addRRect(RRect.fromRectAndCorners(
              Rect.fromCenter(
                  center: Offset(center.dx + offsetPos.dx - 3,
                      center.dy + offsetPos.dy - 3),
                  height: height,
                  width: width),
              topLeft: Radius.circular(60),
              topRight: Radius.circular(60),
              bottomLeft: Radius.circular(60),
              bottomRight: Radius.circular(60)))
          ..fillType = PathFillType.evenOdd,
        Paint()
          ..color = darkGrey.brighten(18).color);
         // ..maskFilter =
         //     MaskFilter.blur(BlurStyle.normal, convertRadiusToSigma(10)));

    //canvas.drawPath(path, paint);
//    canvas.drawRect(Rect.fromCenter(center: Offset(center.dx - 3, center.dy - size.height/3 - 3),
//        height: 50, width: 50),
//        Paint()
//          ..color = lightGrey.color
//          ..maskFilter = MaskFilter.blur(BlurStyle.normal, convertRadiusToSigma(20)));
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class RotarySlider extends StatefulWidget {
  final ValueChanged<int> changedCallBack;
  int radius;

  RotarySlider({
    this.changedCallBack,
    this.radius
  });

  @override
  _RotarySliderState createState() => _RotarySliderState();
}

class _RotarySliderState extends State<RotarySlider> {
  double currentAngle;
  Offset currentOffset;
  double lastVal;

  @override
  void initState() {
    currentAngle = 0;
    currentOffset = calcOffsetPos(0);
    lastVal = 0.0;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
//      onPanDown: _onPanDown,
      onPanUpdate: _panHandler,
//      onPanEnd: _onPanEnd,
      child: Stack(
        children: <Widget>[
          CustomPaint(
            painter: BasePainter(),
            foregroundPainter:
                SliderPainter(width: 58, height: 58, offsetPos: currentOffset),
            child: Center(
              child: Container(
                height: 200,
                width: 200,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Offset calcOffsetPos(double angle) {
    double rad = angle * pi / 180;
    double x = cos(rad) * widget.radius;
    double y = sin(rad) * widget.radius;
    return Offset(x, y);
  }

  double calculateAngle(double dx, double dy) {

    double l = sqrt(pow(dx, 2) + pow(dy, 2));
    l *= dy >= 0 ? -1 : 1;
    double sin_angle = l / (2 * widget.radius);
    double angle_r = asin(sin_angle);

    return angle_r * 180 / pi;
  }

  void _onPanUpdate(DragUpdateDetails details) {
    //RenderBox renderBox = context.findRenderObject();
    //var position = renderBox.globalToLocal(details);

    //double angle = de
    setState(() {
      //  debugPrint("dx ${details.delta.dx.toString()}; dy ${details.delta.dy.toString()}");
        double angle = calculateAngle(details.delta.dx, details.delta.dy);
        currentAngle += angle;
        currentOffset = calcOffsetPos(currentAngle);
        widget.changedCallBack(angle > 0 ? 1 : -1);
    });
  }

  void _panHandler(DragUpdateDetails d) {

    /// Pan location on the wheel
    bool onTop = d.localPosition.dy <=  widget.radius;
    bool onLeftSide = d.localPosition.dx <=  widget.radius;
    bool onRightSide = !onLeftSide;
    bool onBottom = !onTop;

    /// Pan movements
    bool panUp = d.delta.dy <= 0.0;
    bool panLeft = d.delta.dx <= 0.0;
    bool panRight = !panLeft;
    bool panDown = !panUp;

    /// Absoulte change on axis
    double yChange = d.delta.dy.abs();
    double xChange = d.delta.dx.abs();

    /// Directional change on wheel
    double verticalRotation = (onRightSide && panDown) || (onLeftSide && panUp)
        ? yChange
        : yChange * -1;

    double horizontalRotation = (onTop && panRight) || (onBottom && panLeft)
        ? xChange
        : xChange * -1;

    // Total computed change
    double rotationalChange;

//    if(d.delta.distance < 2)
//      rotationalChange = (verticalRotation + horizontalRotation) * 0.2;
//    else if (d.delta.distance > 3
//    )
      rotationalChange = (verticalRotation + horizontalRotation) ;
//    else
//      rotationalChange = (verticalRotation + horizontalRotation) * 0.5 * 3;

    bool movingClockwise = rotationalChange > 0;
    bool movingCounterClockwise = rotationalChange < 0;


    // Now do something interesting with these computations!
    debugPrint(d.delta.distance.toString());

    double delta = rotationalChange.abs();

//    if (d.delta.distance > 1)
//      delta = 2.0;

    if (movingClockwise)
      currentAngle += delta;
    else
      currentAngle -= delta;

    setState(() {
      currentOffset = calcOffsetPos(currentAngle);
    });

   // if(d.delta.distance >= 0.2)
    widget.changedCallBack(rotationalChange > 0 ? 1 : -1);
    //widget.changedCallBack(rotationalChange.toInt());
  }
}
